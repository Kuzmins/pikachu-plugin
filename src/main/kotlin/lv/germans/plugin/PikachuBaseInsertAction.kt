package lv.germans.plugin

import com.intellij.openapi.actionSystem.*
import com.intellij.openapi.command.WriteCommandAction
import com.intellij.psi.PsiDocumentManager
import com.intellij.psi.PsiElement
import com.intellij.psi.PsiMethod
import com.intellij.psi.codeStyle.CodeStyleManager
import com.intellij.psi.util.PsiTreeUtil

abstract class PikachuBaseInsertAction : AnAction() {

    abstract fun shouldShowActionBasedOnPsiElement(event: AnActionEvent, element: PsiElement): Boolean

    abstract fun prepareInsertStatement(ascii: List<String>): String

    open fun getElementToReformat(event: AnActionEvent, element: PsiElement): PsiElement? = null

    override fun actionPerformed(e: AnActionEvent) {
        val editor = e.getData(PlatformDataKeys.EDITOR) ?: return
        val caretOffset = editor.caretModel.offset
        val psiFile = e.getData(LangDataKeys.PSI_FILE) ?: return
        val project = editor.project ?: return

        val manager = PsiDocumentManager.getInstance(e.project!!)
        val document = manager.getDocument(psiFile) ?: return
        val element = psiFile.findElementAt(caretOffset) ?: return

        val service = project.getService(PikachuAsciiService::class.java)
        val insertStatement = prepareInsertStatement(service.getPikachuAsciiStrings())
        val elementToReformat = getElementToReformat(e, element)

        WriteCommandAction.runWriteCommandAction(editor.project) {
            document.insertString(caretOffset, insertStatement)
            manager.commitDocument(document)

            elementToReformat?.let {
                CodeStyleManager.getInstance(project).reformat(it)
            }
        }
    }

    override fun update(e: AnActionEvent) {
        e.presentation.isEnabledAndVisible = shouldShowAction(e)
    }

    private fun shouldShowAction(e: AnActionEvent): Boolean {
        val editor = e.getData(CommonDataKeys.EDITOR) ?: return false
        val psiFile = e.getData(LangDataKeys.PSI_FILE) ?: return false
        val caretOffset = editor.caretModel.offset
        val element = psiFile.findElementAt(caretOffset) ?: return false
        return shouldShowActionBasedOnPsiElement(e, element)
    }

}