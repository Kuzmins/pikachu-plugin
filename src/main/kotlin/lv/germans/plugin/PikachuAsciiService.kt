package lv.germans.plugin

import com.intellij.openapi.components.Service
import com.intellij.openapi.project.Project
import java.io.File

@Service
class PikachuAsciiService(val myProject: Project) {

    fun getPikachuAsciiStrings(): List<String> {
        val asciiString = this::class.java.getResourceAsStream("/ascii-art.txt").readBytes().toString(Charsets.UTF_8)
        return asciiString.lines()
    }
}